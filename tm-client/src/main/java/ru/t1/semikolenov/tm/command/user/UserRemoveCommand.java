package ru.t1.semikolenov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.UserRemoveRequest;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    public static final String DESCRIPTION = "Remove user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        @NotNull String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}