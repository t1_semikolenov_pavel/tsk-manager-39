package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}