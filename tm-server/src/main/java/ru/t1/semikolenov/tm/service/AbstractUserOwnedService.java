package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.repository.IUserOwnedRepository;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.IUserOwnedService;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}