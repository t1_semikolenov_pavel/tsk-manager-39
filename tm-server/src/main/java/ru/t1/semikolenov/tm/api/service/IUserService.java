package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role);

    void add(@NotNull User user);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    User updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}