package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    void create(@NotNull String userId, @NotNull String name);

    void create(@NotNull String userId, @NotNull String name, @NotNull String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

}