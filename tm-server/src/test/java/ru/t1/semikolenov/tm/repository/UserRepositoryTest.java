package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.semikolenov.tm.api.repository.IUserRepository;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;

import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

//    @NotNull
//    private final IUserRepository userRepository = new UserRepository();
//
//    private final static long INITIAL_SIZE = 5;
//
//    @Before
//    public void init() {
//        for (int i = 0; i < INITIAL_SIZE; i++) {
//            @NotNull final User user = new User();
//            user.setLogin("user-" + i);
//            user.setEmail("user-" + i + "@t1-consulting.ru");
//            user.setRole(Role.USUAL);
//            userRepository.add(user);
//        }
//    }
//
//    @Test
//    public void add() {
//        @NotNull final User user = new User();
//        userRepository.add(user);
//        Assert.assertEquals(INITIAL_SIZE + 1, userRepository.getSize());
//        Assert.assertTrue(userRepository.findAll().contains(user));
//    }
//
//    @Test
//    public void clear() {
//        userRepository.clear();
//        Assert.assertEquals(0, userRepository.getSize());
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<User> users = userRepository.findAll();
//        Assert.assertEquals(INITIAL_SIZE, users.size());
//    }
//
//    @Test
//    public void findOneById() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        Assert.assertNotNull(userRepository.findOneById(userId));
//        Assert.assertNull(userRepository.findOneById(UUID.randomUUID().toString()));
//    }
//
//    @Test
//    public void findOneByIndex() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        @NotNull final Integer userIndex = 5;
//        Assert.assertNotNull(userRepository.findOneByIndex(userIndex));
//        Assert.assertEquals(userId, userRepository.findOneByIndex(userIndex).getId());
//    }
//
//    @Test
//    public void existsById() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        Assert.assertTrue(userRepository.existsById(userId));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        Assert.assertEquals(INITIAL_SIZE + 1, userRepository.getSize());
//        userRepository.remove(user);
//        Assert.assertNull(userRepository.findOneById(userId));
//        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        Assert.assertEquals(INITIAL_SIZE + 1, userRepository.getSize());
//        userRepository.removeById(userId);
//        Assert.assertNull(userRepository.findOneById(userId));
//        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
//    }
//
//    @Test
//    public void removeByIndex() {
//        @NotNull final User user = new User();
//        @NotNull final String userId = user.getId();
//        userRepository.add(user);
//        Assert.assertEquals(INITIAL_SIZE + 1, userRepository.getSize());
//        @NotNull final Integer userIndex = 5;
//        userRepository.removeByIndex(userIndex);
//        Assert.assertNull(userRepository.findOneById(userId));
//        Assert.assertThrows(IndexOutOfBoundsException.class,
//                () -> userRepository.findOneByIndex(userIndex));
//        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
//    }
//
//    @Test
//    public void findOneByLogin() {
//        for (int i = 0; i < INITIAL_SIZE; i++) {
//            Assert.assertNotNull(userRepository.findOneByLogin("user-" + i));
//        }
//        Assert.assertNull(userRepository.findOneByLogin("test-find-login"));
//    }
//
//    @Test
//    public void findOneByEmail() {
//        for (int i = 0; i < INITIAL_SIZE; i++) {
//            Assert.assertNotNull(userRepository.findOneByEmail("user-" + i + "@t1-consulting.ru"));
//        }
//        Assert.assertNull(userRepository.findOneByEmail("test-find-email"));
//    }
//
//    @Test
//    public void isLoginExist() {
//        for (int i = 0; i < INITIAL_SIZE; i++) {
//            Assert.assertTrue(userRepository.isLoginExist("user-" + i));
//        }
//        Assert.assertFalse(userRepository.isLoginExist("test-find-login"));
//    }
//
//    @Test
//    public void isEmailExist() {
//        for (int i = 0; i < INITIAL_SIZE; i++) {
//            Assert.assertTrue(userRepository.isEmailExist("user-" + i + "@t1-consulting.ru"));
//        }
//        Assert.assertFalse(userRepository.isEmailExist("test-find-email"));
//    }

}
